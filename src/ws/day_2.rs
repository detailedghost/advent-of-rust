use std::fs::read_to_string;

#[derive(Debug)]
enum Strategy {
    Rock,
    Paper,
    Scissors,
}

fn gen_action_map(contents: &str) -> Vec<Vec<Option<Strategy>>> {
    contents
        .lines()
        .filter(|target| target.len() > 0)
        .map(|x| {
            x.split(" ")
                .map(|a| -> Option<Strategy> {
                    match a.to_uppercase().chars().next() {
                        Some('A') | Some('X') => Some(Strategy::Rock),
                        Some('B') | Some('Y') => Some(Strategy::Paper),
                        Some('C') | Some('Z') => Some(Strategy::Scissors),
                        _ => None,
                    }
                })
                .collect()
        })
        .collect()
}

pub fn exercise(path: &str) {
    let contents = read_to_string(path).expect("Require valid file");
    let translated = &gen_action_map(&contents);
    let score = translated.iter().fold(0, |acc, x| {
        let selection = match x[1].as_ref().unwrap() {
            Strategy::Rock => 1,
            Strategy::Paper => 2,
            Strategy::Scissors => 3
        };
        let match_result = match (x[0].as_ref().unwrap(), x[1].as_ref().unwrap()) {
            (Strategy::Rock, Strategy::Scissors) => 0,
            (Strategy::Paper, Strategy::Rock) => 0,
            (Strategy::Scissors, Strategy::Paper) => 0,
            (Strategy::Rock, Strategy::Rock) => 3,
            (Strategy::Paper, Strategy::Paper) => 3,
            (Strategy::Scissors, Strategy::Scissors) => 3,
            (Strategy::Rock, Strategy::Paper) => 6,
            (Strategy::Paper, Strategy::Scissors) => 6,
            (Strategy::Scissors, Strategy::Rock) => 6,
        };
        acc + selection + match_result
    });
    println!("{:?}", score)
}
