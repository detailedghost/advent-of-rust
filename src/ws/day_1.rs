use std::fs::read_to_string;

fn get_calorie_map(path: &str) -> Result<Vec<u32>, String> {
    println!("Reading from {}", path);
    let data: String = read_to_string(path).expect("Should be able to find file");

    let content: Vec<&str> = data.lines().collect();

    let mut map: Vec<u32> = Vec::new();
    map.push(0);
    let mut pos: usize = 0;
    for item in content {
        if item.is_empty() {
            map.push(0);
            pos += 1;
            continue;
        }
        map[pos] += item.parse::<u32>().unwrap();
    }
    return Ok(map);
}

pub fn exercise(path: &str) {
    let mut index: i32 = -1;
    let mut elf_calories: Vec<_> = get_calorie_map(path)
        .unwrap()
        .iter()
        .map(|&x| -> (i32, u32) {
            index += 1;
            return (index, x);
        })
        .collect();
    elf_calories.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
    elf_calories.reverse();
    println!("Elf #{:?} that has the highest count", elf_calories[0].0)
}
