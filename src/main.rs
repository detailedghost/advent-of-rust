use std::env::args;

pub mod ws;

fn main() {
    let args: Vec<String> = args().collect();
    let day_flag = &args[1];
    let day_sel = &args[2];
    if (day_flag == "-day" || day_flag == "--day") && !day_sel.is_empty() {
        println!("{}", day_sel);
        match &day_sel[..] {
            "1" => ws::day_1::exercise("./static/calories.txt"),
            "2" => ws::day_2::exercise("./static/rpc.txt"),
            _ => println!("Not available yet")
        }
    }
}
